<?php

namespace App\Exceptions\Category;

use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class CategoryUpdateException extends Exception
{
    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report()
    {
        Log::debug('CategoryUpdateException report worked!');
    }

    public function render()
    {
        return Response::json([
            'success' => false,
            'message' => 'Unhandled Error on updating Category on Database'
        ], 500);
    }
}
