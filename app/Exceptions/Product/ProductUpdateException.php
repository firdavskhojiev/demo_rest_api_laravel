<?php

namespace App\Exceptions\Product;

use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class ProductUpdateException extends Exception
{
    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report()
    {
        Log::debug('ProductUpdateException report worked!');
    }

    public function render()
    {
        return Response::json([
            'success' => false,
            'message' => 'Unhandled Error on updating Product on Database'
        ], 500);
    }
}
