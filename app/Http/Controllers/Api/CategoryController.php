<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\Category\CategoryStoreException;
use App\Exceptions\Category\CategoryUpdateException;
use App\Http\Requests\Category\CategoryStoreRequest;
use App\Http\Requests\Category\CategoryUpdateRequest;
use App\Models\Api\Category;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Exception;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $categories = Category::all();

        return Response::json([
            'status' => true,
            'categories' => $categories,
        ], 200);
    }

    public function productsByCategory(Category $category)
    {
        return Response::json([
            'status' => true,
            'category_id' => $category->id,
            'products' => $category->products,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Category\CategoryStoreRequest  $request
     * @throws CategoryStoreException throws if error by query
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CategoryStoreRequest $request)
    {
        $name = $request->input('name');
        $description = $request->input('description');

        try {
            $category = new Category();
            $category->name = $name;
            $category->description = $description;
            $category->save();
        } catch (QueryException $ex) {
            throw new CategoryStoreException();
        } catch (Exception $ex) {
            return Response::json([
                'status' => false,
                'message' => 'Unhandled error'
            ], 500);
        }

        return Response::json([
            'status' => true,
            'message' => 'Category Successfully created',
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Api\Category  $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Category $category)
    {
        return Response::json([
            'status' => true,
            'category' => $category,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Category\CategoryUpdateRequest $request
     * @param  \App\Models\Api\Category  $category
     * @throws CategoryUpdateException throws if error by query
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CategoryUpdateRequest $request, Category $category)
    {
        try {
            if ($request->has('name')) {
                $category->name = $request->input('name');
            }

            if ($request->has('description')) {
                $category->description = $request->input('description');
            }

            $category->save();
        } catch (QueryException  $ex) {
            throw new CategoryUpdateException();
        } catch (Exception $ex) {
            return Response::json([
                'status' => false
            ], 500);
        }

        return Response::json([
            'status' => true,
            'message' => 'Category Successfully updated',
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Api\Category  $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return Response::json([
            'status' => true,
            'message' => 'Category Successfully deleted',
        ], 200);
    }
}
