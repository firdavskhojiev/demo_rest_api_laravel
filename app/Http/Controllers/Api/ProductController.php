<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\Product\ProductStoreException;
use App\Exceptions\Product\ProductUpdateException;
use App\Http\Requests\Product\ProductStoreRequest;
use App\Http\Requests\Product\ProductUpdateRequest;
use App\Models\Api\Product;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $products = Product::all();

        return Response::json([
            'status' => true,
            'products' => $products,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Product\ProductStoreRequest  $request
     * @throws ProductStoreException throws if error by query
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProductStoreRequest $request)
    {
        $category_id = $request->input('category_id');
        $name = $request->input('name');
        $price = $request->input('price');

        try {
            $product = new Product();
            $product->category_id = $category_id;
            $product->name = $name;
            $product->price = $price;
            $product->save();
        } catch (QueryException $ex) {
            throw new ProductStoreException();
        } catch (Exception $ex) {
            return Response::json([
                'status' => false,
                'message' => 'Unhandled error'
            ], 500);
        }

        return Response::json([
            'status' => true,
            'message' => 'Product Successfully created',
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Api\Product  $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Product $product)
    {
        return Response::json([
            'status' => true,
            'product' => $product,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Product\ProductUpdateRequest  $request
     * @param  \App\Models\Api\Product  $product
     * @throws ProductUpdateException throws if error by query
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ProductUpdateRequest $request, Product $product)
    {
        try {
            if ($request->has('category_id')) {
                $product->category_id = $request->input('category_id');
            }

            if ($request->has('name')) {
                $product->name = $request->input('name');
            }

            if ($request->has('price')) {
                $product->price = $request->input('price');
            }

            $product->save();
        } catch (QueryException  $ex) {
            throw new ProductUpdateException();
        } catch (Exception $ex) {
            return Response::json([
                'status' => false
            ], 500);
        }

        return Response::json([
            'status' => true,
            'message' => 'Product Successfully updated',
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Api\Product  $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return Response::json([
            'status' => true,
            'message' => 'Product Successfully deleted',
        ], 200);
    }
}
